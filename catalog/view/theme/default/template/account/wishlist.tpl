<?php echo $header; ?>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <?php if ($success) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-md-9 col-sm-12'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="account-content <?php echo $class; ?>"><?php echo $content_top; ?>
      <h2><?php echo $heading_title; ?></h2>
      <?php if ($products) { ?>
      <div class="row">

          <?php foreach ($products as $product) { ?>
              <div class="product-layout product-list col-md-4 col-sm-4 col-xs-6">
                  <div class="product-thumb">
                      <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>
                      <div class="caption">
                          <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
                          <?php if ($product['price']) { ?>
                              <p class="price">
                                  <?php if (!$product['special']) { ?>
                                      <?php echo $product['price']; ?>
                                  <?php } else { ?>
                                      <span class="price-old"><?php echo $product['price']; ?></span> <span class="price-new"><?php echo $product['special']; ?></span>
                                  <?php } ?>
                              </p>
                          <?php } ?>
                      </div>
                      <div class="button-groups">
                          <button class="cart" type="button" onclick="cart.add('<?php echo $product['product_id']; ?>');"><i class="icon-i-cart"></i> <span class=""><?php echo $button_cart; ?></span></button>
                          <button class="dop-prod" type="button"  onclick="compare.add('<?php echo $product['product_id']; ?>');"><i class="icon-i-scale"></i></button>
                          <a href="<?php echo $product['remove']; ?>"  class="dop-prod"><i class="fa fa-times"></i></a>
                      </div>
                  </div>
              </div>
          <?php } ?>
      </div>
      <?php } else { ?>
      <p><?php echo $text_empty; ?></p>
      <?php } ?>
      <div class="buttons clearfix">
        <div class="pull-right"><a href="/" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
      </div>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>