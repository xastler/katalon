<?php echo $header; ?>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-md-9 col-sm-12'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="account-content <?php echo $class; ?>"><?php echo $content_top; ?>
      <h2><?php echo $heading_title; ?></h2>
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" >
        <fieldset>
          <div class="account-form-group">
            <label class="label-block control-label"><?php echo $entry_newsletter; ?></label>
            <div class="input-block">
              <?php if ($newsletter) { ?>
                  <div class="input-block-radio">
                      <input type="radio" name="newsletter" value="1" id="in-no" checked="checked" />
                      <label class="radio-inline"  for="in-no"></label>
                      <?php echo $text_yes; ?>

                  </div>
                  <div class="input-block-radio">
                      <input type="radio" name="newsletter" value="0"   id="in-yes" />
                      <label class="radio-inline" for="in-yes"> </label>
                      <?php echo $text_no; ?>
                  </div>
              <?php } else { ?>
                <div class="input-block-radio">
                    <input type="radio" name="newsletter" value="1" id="in-no" />
                    <label class="radio-inline"  for="in-no"></label>
                    <?php echo $text_yes; ?>

                </div>
                <div class="input-block-radio">
                    <input type="radio" name="newsletter" value="0" checked="checked"  id="in-yes" />
                    <label class="radio-inline" for="in-yes"> </label>
                    <?php echo $text_no; ?>
                </div>
              <?php } ?>
            </div>
          </div>
        </fieldset>
        <div class="buttons clearfix">
          <div class="pull-left">
            <input type="submit" value="СОХРАНИТЬ" class="btn-stl" />
          </div>
        </div>
      </form>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>