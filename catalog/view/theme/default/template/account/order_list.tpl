<?php echo $header; ?>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-md-9 col-sm-12'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="account-content <?php echo $class; ?>"><?php echo $content_top; ?>
      <h2><?php echo $heading_title; ?></h2>
      <?php if ($orders) { ?>
      <div class="order-history">
        <table class="order-history-table">
          <thead>
            <tr>
              <td class=""><?php echo $column_order_id; ?></td>
              <td class=""><?php echo $column_status; ?></td>
              <td class=""><?php echo $column_date_added; ?></td>
              <td class=""><?php echo $column_product; ?></td>
              <td class=""><?php echo $column_customer; ?></td>
              <td class=""><?php echo $column_total; ?></td>
                <td></td>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($orders as $order) { ?>
            <tr>
              <td class="">#<?php echo $order['order_id']; ?></td>
              <td class=""><?php echo $order['status']; ?></td>
              <td class=""><?php echo $order['date_added']; ?></td>
              <td class=""><?php echo $order['products']; ?></td>
              <td class=""><?php echo $order['name']; ?></td>
              <td class=""><?php echo $order['total']; ?></td>
                <td class=""><a href="<?php echo $order['href']; ?>" class="btn btn-info"><i class="fa fa-eye"></i></a></td>

            </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
      <div class="text-right"><?php echo $pagination; ?></div>
      <?php } else { ?>
      <p><?php echo $text_empty; ?></p>
      <?php } ?>

      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>