<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if IE 10 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie10"><![endif]-->
<!--[if (gt IE 10)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<!--[if lt IE 10]>
<link rel="stylesheet" href="/reject/reject.css" media="all" />
<script type="text/javascript" src="/reject/reject.min.js"></script>
<![endif]-->
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php echo $title; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?></title>
<base href="<?php echo $base; ?>" />
<?php if (isset($robots) && $robots) { ?>
<meta name="robots" content="<?php echo $robots; ?>"/>
<?php } ?>
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content= "<?php echo $keywords; ?>" />
<?php } ?>
<meta property="og:title" content="<?php echo $title; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?>" />
<meta property="og:type" content="website" />
<meta property="og:url" content="<?php echo $og_url; ?>" />
<?php if ($og_image) { ?>
<meta property="og:image" content="<?php echo $og_image; ?>" />
<?php } else { ?>
<meta property="og:image" content="<?php echo $logo; ?>" />
<?php } ?>
<meta property="og:site_name" content="<?php echo $name; ?>" />

<meta property="url" content="<?php echo $og_url; ?>">
<meta property="title" content="<?php echo $title; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?>" />
<?php if ($description) { ?>
<meta property="description" content="<?php echo $description; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?>">
<?php } ?>
<?php if ($og_image) { ?>
<meta property="image" content="<?php echo $og_image; ?>" />
<?php } else { ?>
<meta property="image" content="<?php echo $logo; ?>" />
<?php } ?>
<meta itemprop="url" content="<?php echo $og_url; ?>">
<meta itemprop="name" content="<?php echo $title; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?>">
<?php if ($description) { ?>
<meta itemprop="description" content="<?php echo $description; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?>">
<?php } ?>
<?php if ($og_image) { ?>
<meta itemprop="image" content="<?php echo $og_image; ?>" />
<?php } else { ?>
<meta itemprop="image" content="<?php echo $logo; ?>" />
<?php } ?>
    <?php if($gtm) { ?>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?
id='+i+dl;f.parentNode.insertBefore(j,f);
 })(window,document,'script','dataLayer','<?php echo $gtm; ?>');</script>
<!-- End Google Tag Manager -->
    <?php } ?>
<link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />
<link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,600,700" rel="stylesheet">
<link href="catalog/view/theme/default/stylesheet/stylesheet.css" rel="stylesheet">
<link href="catalog/view/theme/default/stylesheet/main.css" rel="stylesheet">
<link href="catalog/view/theme/default/stylesheet/media.css" rel="stylesheet">
<link href="catalog/view/theme/default/icon-fonts/style.css" rel="stylesheet">
<link href="catalog/view/javascript/jquery/jQueryFormStyler-master/dist/jquery.formstyler.css" rel="stylesheet" />
<link href="catalog/view/javascript/jquery/jQueryFormStyler-master/dist/jquery.formstyler.theme.css" rel="stylesheet" />
<?php foreach ($styles as $style) { ?>
<link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" ></script>
<?php /*
<script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" ></script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCEi_mKbOqvmIL0qXYZEwCZujvacMpBxGQ&callback" ></script>
<script src="catalog/view/javascript/jquery/jQueryFormStyler-master/dist/jquery.formstyler.min.js"></script>
<script src="catalog/view/javascript/jquery/jquery.maskedinput.min.js"></script>
<script src="catalog/view/javascript/common.js"></script>*/?>
<?php foreach ($scripts as $script) { ?>
<script src="<?php echo $script; ?>"></script>
<?php } ?>

<?php foreach ($analytics as $analytic) { ?>
<?php echo $analytic; ?>
<?php } ?>
<script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "Organization",
        "url" : "<?php echo $base; ?>",
        "telephone": "<?php echo $telephone1; ?>",
        "name" : "<?php echo $title;
    if (isset($_GET['page'])) {
        echo ' - ' . ((int)$_GET['page']) . ' ' . $text_page;
    } ?>",
        "logo" : "<?php echo $logo; ?>",
        "description" : "<?php echo $name; ?>",
        "contactPoint": [
        { "@type": "ContactPoint",
          "telephone": "<?php echo $telephone1; ?>",
          "contactType": "customer support"
        }
      ],
      "address": [
        { "@type": "PostalAddress",
          "streetAddress": "<?php echo $address; ?>"
        }
      ]
    }
</script>
</head>
<body class="<?php echo $class; ?>">
<?php if($gtm) { ?>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=<?php echo $gtm; ?>>"
                      height="0" width="0"
                      style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
<?php } ?>
<div class="top_bar">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-sm-2 col-xs-3">
                <div class="top_bar-btn"><span></span><span></span><span></span></div>
                <div class="top_bar-menu">
                    <ul class="top_bar-list">
                        <?php $top_bar='active'; ?>
                        <li class="<?php if ($special == $og_url) { echo $top_bar; }?>"><a href="<?php echo $special; ?>"><?php echo $text_special; ?></a></li>
                        <?php if ($informations) { ?>
                            <?php foreach ($informations as $information) { ?>
                                <li class="<?php if ($information['href'] == $og_url) { echo $top_bar; }?>"><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
                            <?php } ?>
                        <?php } ?>
                        <li class="<?php if ($news == $og_url) { echo $top_bar; }?>"><a href="<?php echo $news; ?>"><?php echo $text_news; ?></a></li>
                        <li class="<?php if ($contact == $og_url) { echo $top_bar; }?>"><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?> </a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-2  col-sm-10 col-xs-9" id="top_bar-logout">
                <div class="top_bar-logout">
                    <?php if ($logged) { ?>
                        <a href="<?php echo $account; ?>" title="<?php echo $text_account; ?>">Личний кабинет</a>
                    <?php }else{?>
                        <a class="quick_login">Войти / Регистрация</a>
                    <?php }?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo $quicksignup; ?>
<header class="header">
  <div class="container">
      <div class="row">
          <div class="col-lg-3 col-md-6 col-sm-6">
              <div id="logo">
                  <?php if ($logo) { ?>
                      <?php if ($home == $og_url) { ?>
                          <img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" />
                      <?php } else { ?>
                          <a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" /></a>
                      <?php } ?>
                  <?php } else { ?>
                      <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
                  <?php } ?>
              </div>
          </div>
          <div class="col-lg-3 col-md-6 col-sm-6">
              <?php echo $search; ?>
              <div class="header-email"><i class="icon-i-mail"></i> <a href="email:<?php echo $email ?>"><?php echo $email ?></a></div>
          </div>
          <div class="col-lg-2 col-md-3 col-sm-4 col-xs-4">
            <div class="header-work_time">
                <i class="icon-i-clock"></i>
                <?php echo $data_work ?>
            </div>
          </div>
          <div class="col-lg-2 col-md-6 col-sm-4 col-xs-4">
              <div class="header-phone">
                  <?php if ($telephone1) { ?>
                      <a class="header-phone-main" href="tel:<?php echo preg_replace("/[ ()-]/", "", $telephone1) ;?>">
                          <i class="icon-i-lifecell"></i><?php echo $telephone1; ?>
                      </a>
                  <?php }?>
                  <?php if ($telephone2) { ?>
                      <a class="header-phone-kv" href="tel:<?php echo preg_replace("/[ ()-]/", "", $telephone2) ;?>">
                          <i class="icon-i-ks"></i>
                          <?php echo $telephone2; ?>
                      </a>
                  <?php }?>
                  <?php if ($telephone3) { ?>
                      <a class="header-phone-mts" href="tel:<?php echo preg_replace("/[ ()-]/", "", $telephone3) ;?>">
                          <i class="icon-i-mts"></i>
                          <?php echo $telephone3; ?>
                      </a>
                  <?php }?>
                  <?php if ($telephone4) { ?>
                      <a class="header-phone-life" href="tel:<?php echo preg_replace("/[ ()-]/", "", $telephone4) ;?>">
                          <i class="icon-i-phone"></i>
                          <?php echo $telephone4; ?>
                      </a>
                  <?php }?>
              </div>
          </div>
          <div class="col-lg-2 col-md-3 col-sm-4 col-xs-4">
              <?php echo $cart; ?>
              <div class="header-wish">
                  <a href="<?php echo $compare ; ?>">
                      <i class="icon-i-scale"></i>
                      <span class="num-select"><?php echo $text_compare; ?></span>
                  </a>
                  <a href="<?php echo $wishlist; ?>">
                      <i class="icon-i-heart"></i>
                      <span class="num-select"><?php echo $text_wishlist; ?></span>
                  </a>
              </div>
          </div>
      </div>
  </div>
</header>
<?php if ($categories) { ?>
<div class="categories_menu">
    <div class="container">
        <div class="categories_menu-btn">категории <div class="categories_menu-btn-span"><span></span><span></span><span></span></div></div>
        <div class="categories_menu-block">
          <ul class="categories_menu-list">
            <?php foreach ($categories as $category) { ?>
            <?php if ($category['children']) { ?>
            <li class="dropdown"><a href="<?php echo $category['href']; ?>" class="dropdown-toggle" data-toggle="dropdown"><?php echo $category['name']; ?><i class="icon-drop-down"></i></a>
              <div class="dropdown-menu">
                <div class="dropdown-inner">
                  <?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?>
                  <ul class="list-unstyled">
                    <?php foreach ($children as $child) { ?>
                    <li><a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a></li>
                    <?php } ?>
                  </ul>
                  <?php } ?>
                </div>
                <a href="<?php echo $category['href']; ?>" class="see-all"><?php echo $text_all; ?> <?php echo $category['name']; ?></a> </div>
            </li>
            <?php } else { ?>
            <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
            <?php } ?>
            <?php } ?>
          </ul>
        </div>
    </div>
</div>
<?php } ?>
