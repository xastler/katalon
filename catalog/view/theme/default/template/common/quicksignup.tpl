
<div class="modal fade" id="modal-login" tabindex="-1" role="dialog">
    <div class="modal-dialog" >
		<div class="modal-content  popups_log" >
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <div class="popups_log-header">
                <ul>
                    <li data-href="log" class="active">АВТОРИЗАЦИЯ</li>
                    <li data-href="reg">РЕГИСТРАЦИЯ</li>
                </ul>
            </div>
            <div class="popups_log-content">
                <div class="popups_log-form log active" id="quick-login">
                    <div class="form-group required">
                        <input type="text" name="email" value=""  id="input-email-l" class="form-control" placeholder="<?php echo $entry_email; ?>" />
                    </div>
                    <div class="form-group required">
                        <input type="password" name="password" value="" id="input-password-l" class="form-control password" placeholder="<?php echo $entry_password; ?>" />
                    </div>
                    <div class="form-group form-groupss checkbox22">
                        <div class="check_box">
                            <input type="checkbox" class="checkbox" id="checkbox22" />
                            <label for="checkbox22">Показать пароль</label>
                        </div>

                        <a href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a>
                    </div>
                    <button type="button" class="btn-logs loginaccount"  data-loading-text="<?php echo $text_loading; ?>"><?php echo $button_login ?></button>
                    <p>Войти с помощью соц. сетей</p>
                    <div class="popups_log-social">
                        <a href="1" class="icon-i-fb"></a>
                        <a href="2" class="icon-google-plus"></a>
                    </div>
                </div>
                <div class="popups_log-form reg" id="quick-register">
                    <div class="form-group required">
                        <input type="text" name="name" value="" id="input-name-r" class="form-control" placeholder="<?php echo $entry_name; ?>" />
                    </div>
                    <div class="form-group required">
                        <input type="text" name="email" value="" id="input-email-r" class="form-control" placeholder="<?php echo $entry_email; ?>" />
                    </div>
                    <div class="form-group required">
                        <input type="text" name="telephone" value="" id="input-telephone-r" class="form-control" placeholder="<?php echo $entry_telephone; ?>" />
                    </div>
                    <div class="form-group required">
                        <input type="password" name="password" value="" id="input-password-r" class="form-control password" placeholder="<?php echo $entry_password; ?>" />
                    </div>
                    <div class="form-group form-groupss checkbox33">
                        <div class="check_box">
                            <input type="checkbox" class="checkbox" id="checkbox33" />
                            <label for="checkbox33">Показать пароль</label>
                        </div>
                    </div>
                    <?php if ($text_agree) { ?>
                        <input type="checkbox" name="agree" value="1" />&nbsp;<?php echo $text_agree; ?><br><br>
                        <button type="button" class="btn btn-primary createaccount"  data-loading-text="<?php echo $text_loading; ?>" ><?php echo $button_register; ?></button>
                    <?php }else{ ?>
                    <button type="button" class="btn-logs createaccount" data-loading-text="<?php echo $text_loading; ?>" ><?php echo $button_register; ?></button>
                </div>
                <?php } ?>
            </div>
		</div>
	</div>
</div>

