<?php echo $newsletters ?>
<footer class="footer">
  <div class="container">
    <div class="row">
      <?php if ($informations) { ?>
      <div class="col-sm-4">
          <div class="footer-info">
              <div class="footer-logo">
                  <?php if ($logo) { ?>
                      <?php if ($home == $og_url) { ?>
                          <img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" />
                      <?php } else { ?>
                          <a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" /></a>
                      <?php } ?>
                  <?php } else { ?>
                      <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
                  <?php } ?>
              </div>
              <p>В нашем интернет-магазине вы можете купить любую сантехническую продукцию от лучших производителей Украины, Европы и Турции.</p>
              <div class="footer-phone">
                  <?php if ($telephone1) { ?>
                      <a class="header-phone-main" href="tel:<?php echo preg_replace("/[ ()-]/", "", $telephone1) ;?>">
                          <i class="icon-i-lifecell"></i>
                          <?php echo $telephone1; ?>
                      </a>
                  <?php }?>
                  <?php if ($telephone2) { ?>
                      <a class="header-phone-kv" href="tel:<?php echo preg_replace("/[ ()-]/", "", $telephone2) ;?>">
                          <i class="icon-i-ks"></i>
                          <?php echo $telephone2; ?>
                      </a>
                  <?php }?>
                  <?php if ($telephone3) { ?>
                      <a class="header-phone-mts" href="tel:<?php echo preg_replace("/[ ()-]/", "", $telephone3) ;?>">
                          <i class="icon-i-mts"></i>
                          <?php echo $telephone3; ?>
                      </a>
                  <?php }?>
                  <?php if ($telephone4) { ?>
                      <a class="header-phone-life" href="tel:<?php echo preg_replace("/[ ()-]/", "", $telephone4) ;?>">
                          <i class="icon-i-phone"></i>
                          <?php echo $telephone4; ?>
                      </a>
                  <?php }?>
              </div>
              <div class="footer-social">
                  <?php if ($telegram) { ?><a href="<?php echo $telegram; ?>" class="icon-i-insta" target="_blank"></a><?php } ?>
                  <?php if ($fb) { ?><a href="<?php echo $fb; ?>" class="icon-i-fb" target="_blank"></a><?php } ?>
                  <?php if ($vk) { ?><a href="<?php echo $vk; ?>" class="icon-i-vk" target="_blank"></a><?php } ?>
              </div>
          </div>
      </div>
      <?php } ?>
      <div class="col-sm-3 col-xs-4">
          <h5><?php echo $text_information; ?></h5>
          <ul class="list-unstyled">
              <li><a href="<?php echo $special; ?>"><?php echo $text_special; ?></a></li>
              <?php if ($informations) { $k=1;?>
                  <?php foreach ($informations as $information) { ?>
                      <?php if($k>2) {?>
                          <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
                          <?php  }$k++; ?>
                  <?php } ?>
              <?php } ?>
          </ul>
      </div>
      <div class="col-sm-3 col-xs-4">
        <h5><?php echo $text_account; ?></h5>
        <ul class="list-unstyled">
            <li><a href="<?php echo $checkout; ?>"><?php echo $text_checkout; ?></a></li>
            <li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
            <li><a href="<?php echo $compare; ?>"><?php echo $text_compare; ?></a></li>
        </ul>
      </div>
      <div class="col-sm-2 col-xs-4">
        <h5><?php echo $text_extra; ?></h5>
        <ul class="list-unstyled">
            <?php if ($informations) { $k=1;?>
                <?php foreach ($informations as $information) { ?>
                    <?php if($k<3) {?>
                    <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
                <?php $k++; } ?>
                <?php } ?>
            <?php } ?>
            <li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?> </a></li>
        </ul>
      </div>

    </div>

  </div>
</footer>
<div class="copyright">
    <div class="containers">
        <p><?php echo $powered; ?></p>
        <a target="_blank" href="https://web-systems.solutions/"><i class="icon-group-copy"></i></a>
    </div>
</div>
<script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" ></script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCEi_mKbOqvmIL0qXYZEwCZujvacMpBxGQ&callback" ></script>
<script src="catalog/view/javascript/jquery/jQueryFormStyler-master/dist/jquery.formstyler.min.js"></script>
<script src="catalog/view/javascript/jquery/jquery.maskedinput.min.js"></script>
<script src="catalog/view/javascript/common.js"></script>
<script src="catalog/view/javascript/main.js" ></script>

</body></html>