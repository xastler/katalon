<div class="product-layout product-module">
    <div class="product-thumb transition">
        <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>
        <div class="caption">
            <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
            <?php if ($product['price']) { ?>
                <p class="price">
                    <?php if (!$product['special']) { ?>
                        <?php echo $product['price']; ?>
                    <?php } else { ?>
                        <span class="price-old"><?php echo $product['price']; ?></span> <span class="price-new"><?php echo $product['special']; ?></span>
                    <?php } ?>
                </p>
            <?php } ?>
        </div>
        <div class="button-groups">
            <button class="cart" type="button" onclick="cart.add('<?php echo $product['product_id']; ?>');"><i class="icon-i-cart"></i> <span class=""><?php echo $button_cart; ?></span></button>

            <button class="dop-prod" type="button" data-toggle="tooltip" onclick="compare.add('<?php echo $product['product_id']; ?>');"><i class="icon-i-scale"></i></button>
            <button class="dop-prod" type="button" data-toggle="tooltip" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i class="icon-i-heart"></i></button>
        </div>
    </div>
</div>