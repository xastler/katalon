<div class="product-layout product-list col-md-4 col-sm-4 col-xs-6" itemscope itemtype="https://schema.org/Product">
    <meta itemprop="name" content="<?php echo $product['name']; ?>" />
    <meta itemprop="image" content="<?php echo $product['thumb']; ?>" />
    
    <div class="product-thumb">
        <div class="image">
            <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>
        <div class="caption" >
            <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
            <?php if ($product['price']) { ?>
                <p class="price" itemprop="offers" itemscope itemtype="https://schema.org/Offer">
                    <?php if (!$product['special']) { ?>
                        <?php echo $product['price']; ?>
                    <?php } else { ?>
                        <span class="price-old"><?php echo $product['price']; ?></span> <span class="price-new"><?php echo $product['special']; ?></span>
                    <?php } ?>
                    <meta itemprop="priceCurrency" content="UAH" />
                    <meta itemprop="price" content="<?php echo preg_replace("/[^0-9]/", '',$product['price']); ?>" />
                    <meta itemprop="availability" content="http://schema.org/InStock" />
                    <meta itemprop="url" content="<?php echo $product['href']; ?>" />
                </p>
            <?php } ?>
        </div>
        <div class="button-groups">
            <button class="cart" type="button" onclick="cart.add('<?php echo $product['product_id']; ?>');"><i class="icon-i-cart"></i> <span class=""><?php echo $button_cart; ?></span></button>

            <button class="dop-prod" type="button" data-toggle="tooltip" onclick="compare.add('<?php echo $product['product_id']; ?>');"><i class="icon-i-scale"></i></button>
            <button class="dop-prod" type="button" data-toggle="tooltip" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i class="icon-i-heart"></i></button>
        </div>
    </div>
</div>
