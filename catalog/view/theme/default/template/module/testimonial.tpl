<div class="container">
<?php if($heading_title){ ?>
    <h3><?php echo $heading_title; ?></h3>
<?php } ?>
<div class="testimonial_home">
    <div class="testimonial_home-prev icon-arrow-prev"></div>
    <div class="testimonial_home-sliders">
        <?php foreach ($reviews as $review) { ?>
            <div class="testimonial_home-slider">
                <div class="testimonial_home-slider-top">
                    <div class="review-avatar"><?php if($review['image']) {?><img src="<?php echo $review['image']; ?>" alt=""><?php } ?></div>
                    <span class="review-author"><?php echo $review['author']; ?></span>
                </div>
                <div class="testimonial_home-slider-content">
                    <p><?php echo $review['text']; ?></p>
                </div>
                <i class="icon-quotation-mark"></i>
                <i class="icon-quotation-mark-copy"></i>
            </div>
        <?php } ?>
    </div>
    <div class="testimonial_home-next icon-arrow-next"></div>
</div>
</div>
<script>
    $('.testimonial_home-sliders').slick({
        dots: false,
        infinite: true,
        speed: 300,
        slidesToShow: 3,
        slidesToScroll: 1,
        prevArrow: $('.testimonial_home-prev'),
        nextArrow: $('.testimonial_home-next'),
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    });
</script>