<?php if(isset($products)){ ?>
    <div class="watched_products">
        <h3><?php echo $heading_title; ?><?php //echo $text_visitedproduct; ?></h3>
        <div class="featured-slider">
            <div class="featured-prev icon-arrow-prev"></div>
            <div class="slider">
                <?php $i = 0; ?>
                <?php foreach ($products as $product) { ?>
                    <?php $this->partial('product_item_module', array('product' => $product, 'button_cart' => $button_cart, 'button_details' => $button_details));?>
                <?php } ?>
            </div>
            <div class="featured-next icon-arrow-next"></div>
        </div>
    </div>
    <script>
        $('.featured-slider .slider').slick({
            dots: false,
            infinite: true,
            speed: 300,
            slidesToShow: 4,
            slidesToScroll: 1,
            prevArrow: $('.featured-prev'),
            nextArrow: $('.featured-next'),
            responsive: [
                {
                    breakpoint: 1200,
                    settings: {
                        slidesToShow: 3
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 2
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 1
                    }
                }
            ]
        });
    </script>
<?php } ?>