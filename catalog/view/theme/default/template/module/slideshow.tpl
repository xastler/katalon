<div class="slider_home">
    <div class="slider_home-prev icon-arrow-prev"></div>
    <div class="container">
        <div id="slideshow<?php echo $module; ?>" class="" style="opacity: 1;">
          <?php foreach ($banners as $banner) { ?>
          <div class="item">
            <?php if ($banner['link']) { ?>
            <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="img-responsive" />
            <a href="<?php echo $banner['link']; ?>">подробнее</a>
            <?php } else { ?>
            <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="img-responsive" />
            <?php } ?>
          </div>
          <?php } ?>
        </div>
    </div>
    <div class="slider_home-next icon-arrow-next"></div>
</div>
<script>
    $('#slideshow<?php echo $module; ?>').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        prevArrow: $('.slider_home-prev'),
        nextArrow: $('.slider_home-next')
    });
</script>
