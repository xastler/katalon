<div class="container">
<h3><?php echo $heading_title; ?></h3>
<div class="featured-slider">
    <div class="featured-prev icon-arrow-prev"></div>
    <div class="slider">
      <?php foreach ($products as $product) { ?>
          <?php $this->partial('product_item_module', array('product' => $product, 'button_cart' => $button_cart, 'text_tax' => $text_tax, 'button_wishlist' => $button_wishlist, 'button_compare' => $button_compare));?>
      <?php } ?>
    </div>
    <div class="featured-next icon-arrow-next"></div>
</div>
</div>
<script>
    $('.featured-slider .slider').slick({
        dots: false,
        infinite: true,
        speed: 300,
        slidesToShow: 4,
        slidesToScroll: 1,
        prevArrow: $('.featured-prev'),
        nextArrow: $('.featured-next'),
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    });
</script>