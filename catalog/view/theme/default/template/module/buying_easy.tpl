<div class="buying_easy">
    <div class="container">
        <h3>Покупка в Katalano – это просто!</h3>
        <ul>
            <li>
                <i class="icon-i-search"></i>
                <p>Выбираете товары  из каталога</p>
            </li>
            <li>
                <i class="icon-i-select"></i>
                <p>Оформляете заказ</p>
            </li>
            <li>
                <i class="icon-i-delivery"></i>
                <p>Получаете ваши товары с доставкой</p>
            </li>
        </ul>
    </div>
</div>