<div class="newsletter" id="newsletter">
	<div class="container">
        <div class="newsletter-form">
            <div class="newsletter-info">
                <h4><?php echo $heading_title; ?></h4>
                <p><?php echo $text_description; ?></p>
            </div>
            <form id="lt_newsletter_form">
                <input type="email" required name="lt_newsletter_email" id="lt_newsletter_email" placeholder="<?php echo $entry_email; ?>">
                <button type="submit"><i class="icon-right-chevron-1"></i></button>
            </form>
	    </div>
	</div>
</div>
<script><!--
		$(document).ready(function($) {
			$('#lt_newsletter_form').submit(function(){
				$.ajax({
					type: 'post',
					url: '<?php echo $action; ?>',
					data:$("#lt_newsletter_form").serialize(),
					dataType: 'json',
					beforeSend: function() {
						$('.btn-newsletter').attr('disabled', true).button('loading');
					},
					complete: function() {
						$('.btn-newsletter').attr('disabled', false).button('reset');
					},
					success: function(json) {
						$('.newsletter-msg, .text-danger').remove();
						$('.form-group').removeClass('has-error');

						if (json.error) {
							$('#lt_newsletter_form button').after('<div class="newsletter-msg">' + json.error );
						} else {
							$('#lt_newsletter_form button').after('<div class="newsletter-msg">' + json.success     );
							$('#lt_newsletter_email').val('');
						}
					}

				});
				return false;
			});
		});
	//--></script>
