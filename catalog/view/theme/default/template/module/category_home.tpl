<div class="category_home">
    <div class="container">
        <ul>
            <?php $k=1; foreach ($categories as $category) { ?>
                <li>
                    <a href="<?php echo $category['href']; ?>">
                        <div class="category_home-img"><img src="<?php echo $category['thumb']; ?>" alt="category-home <?php echo $category['name']; ?>" class="category_home-main">
                            <img src="./image/catalog/i-<?php echo $k; ?>.png" alt="category-home-2 <?php echo $category['name']; ?>" class="category_home-hover"></div>

                        <span><?php echo $category['name']; ?></span>
                    </a>
                </li>
            <?php $k++;} ?>
        </ul>
    </div>
</div>
