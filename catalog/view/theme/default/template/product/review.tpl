<?php if ($reviews) { ?>
<ul>
    <?php foreach ($reviews as $review) { ?>
        <li>
            <div class="product-review-rating">
            <?php for ($i = 1; $i <= 5; $i++) { ?>
                <?php if ($review['rating'] < $i) { ?>
                    <span class="fa"><i class="icon-star-grey"></i></span>
                <?php } else { ?>
                    <span class="fa"><i class="icon-star-gold"></i></span>
                <?php } ?>
            <?php } ?>
            </div>
            <div class="product-review-name">
                <?php echo $review['author']; ?>
            </div>
            <div class="product-review-text">
                <?php echo $review['text']; ?>
            </div>
        </li>
    <?php } ?>
</ul>
<div class="text-right"><?php echo $pagination; ?></div>
<?php } else { ?>
<p><?php echo $text_no_reviews; ?></p>
<?php } ?>
