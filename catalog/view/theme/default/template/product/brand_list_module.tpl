<?php if ($brands) { ?>
    <div class="container">
        <div class="brands">
            <h3><?php echo $heading_title; ?></h3>
            <div  class="brands-list">
                <div class="row">
                    <?php foreach ($brands as $brand) { ?>
                    <div class="col-sm-3 col-xs-6"><a href="<?php echo $brand['href']; ?>"><?php echo $brand['name']; ?></a></div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
