<?php echo $header; ?>
<div class="container" >
    <?php $this->partial('breadcrumbs', array('breadcrumbs' => $breadcrumbs));?>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?> product"><?php echo $content_top; ?>
<script type="application/ld+json">
{
  "@context": "https://schema.org/",
  "@type": "Product",
  "name": "<?php echo $heading_title; ?>",
  "image": "<?php echo $thumb; ?>",
  "description": "<?php echo $description; ?>",
  "mpn": "<?php echo $sku; ?>",
<?php if ($brand) { ?>
  "brand": {
    "@type": "Thing",
    "name": "<?php echo $brand; ?>"
  },
<?php } ?>
<?php if ($reviews_count) { ?>
  "aggregateRating": {
    "@type": "AggregateRating",
    "ratingValue": "<?=$rating?>",
    "reviewCount": "<?=$reviews_count?>"
  },
<?php } ?>
  "offers": {
    "@type": "Offer",
    "priceCurrency": "UAH",
    "price": "<?php echo preg_replace("/[^0-9]/", '',$price); ?>",
    "availability": "https://schema.org/<?php if($quantity > 0){ ?>InStock<?php } else { ?>OutOfStock<?php } ?>"
  }
}
</script>
      <div class="row">
          <div class="col-sm-6">
              <h1 class="product-name-tel"><?php echo $heading_title; ?></h1>
            <?php if ($thumb || $images) { ?>
                <?php if ($special) { $proc = 100 - ( $special / ( $price / 100 ));?>
                    <div class="product-newx img-newx">
                        -<?php echo round($proc)
                        ;?>%
                    </div>
                <?php }?>
          <ul class="thumbnails">
            <?php if ($thumb) { ?>
            <li><a class="thumbnail" href="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>"><img src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" /></a></li>
            <?php } ?>
            <?php if ($images) { ?>
            <?php foreach ($images as $image) { ?>
            <li class="image-additional"><a class="thumbnail" href="<?php echo $image['popup']; ?>" title="<?php echo $heading_title; ?>"> <img src="<?php echo $image['thumb']; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" /></a></li>
            <?php } ?>
            <?php } ?>
          </ul>
          <?php } ?>
          </div>
          <div class="col-sm-6">

                <h1 class="product-name"><?php echo $heading_title; ?></h1>
                <ul class="list-unstyled product-info">
                    <?php if($category_title){ ?>
                        <li><span><?php echo $text_category; ?></span> <?php echo $category_title; ?></li>
                    <?php } ?>
                    <?php if ($manufacturer) { ?>
                        <li><span><?php echo $text_manufacturer; ?></span> <a href="<?php echo $manufacturers; ?>"><?php echo $manufacturer; ?></a></li>
                    <?php } ?>
                    <?php if ($brand) { ?>
                        <li><span><?php echo $text_brand; ?></span> <a href="<?php echo $brands; ?>"><?php echo $brand; ?></a></li>
                    <?php } ?>
                    <li><span><?php echo $text_model; ?></span> <?php echo $sku; ?></li>
                    <li><span><?php echo $text_stock; ?></span> <?php echo $stock; ?></li>
                </ul>
                <div class="product-price_block">
                    <?php if ($price) { ?>
                        <ul class="list-unstyled">
                            <?php if (!$special) { ?>
                                <li>
                                    <span class="price"><?php echo $price; ?></span>
                                </li>
                            <?php } else { ?>
                                <li><span class="old-price"><?php echo $price; ?></span></li>
                                <li>
                                    <span class="price"><?php echo $special; ?></span>
                                </li>
                            <?php } ?>
                            <?php if ($discounts) { ?>
                                <li>
                                    <hr>
                                </li>
                                <?php foreach ($discounts as $discount) { ?>
                                    <li><?php echo $discount['quantity']; ?><?php echo $text_discount; ?><?php echo $discount['price']; ?></li>
                                <?php } ?>
                            <?php } ?>
                        </ul>
                    <?php } ?>
                    <?php if ($special) {?>
                        <div class="product-inf_new">
                            <?php if ($special) { $proc = 100 - ( $special / ( $price / 100 ));?>
                                <div class="product-newx">
                                    -<?php echo round($proc);?>%
                                </div>
                            <?php }?>
                        </div>
                  <?php }?>
                </div>
                <div class="product-group_block"  id="product">
<!--                        <label class="control-label" for="input-quantity">--><?php //echo $entry_qty; ?><!--</label>-->
                    <div class="product-group_block-num">
                        <span class="num minus">
                            <i class="icon-left-arr"></i>
                        </span>
                        <input type="text" name="quantity" value="<?php echo $minimum; ?>" size="2" id="input-quantity" class="form-control" />
                        <span class="num plus">
                            <i class="icon-right-arr"></i>
                        </span>
                    </div>
                    <input type="hidden" name="product_id" value="<?php echo $product_id; ?>" />
                    <div class="product-group_block-btn">
                        <button type="button" id="button-cart" class="cup" data-loading-text="<?php echo $text_loading; ?>" class=""><i class="icon-i-cart"></i><?php echo $button_cart; ?></button>
                    </div>
                    <?php if (0) {?>
                        <?php if ($options) { ?>
                            <h3><?php echo $text_option; ?></h3>
                            <?php foreach ($options as $option) { ?>
                                <?php if ($option['type'] == 'select') { ?>
                                    <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                        <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                        <select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control">
                                            <option value=""><?php echo $text_select; ?></option>
                                            <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
                                                    <?php if ($option_value['price']) { ?>
                                                        (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                    <?php } ?>
                                                </option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                <?php } ?>
                                <?php if ($option['type'] == 'radio') { ?>
                                    <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                        <label class="control-label"><?php echo $option['name']; ?></label>
                                        <div id="input-option<?php echo $option['product_option_id']; ?>">
                                            <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                                                        <?php echo $option_value['name']; ?>
                                                        <?php if ($option_value['price']) { ?>
                                                            (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                        <?php } ?>
                                                    </label>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php if ($option['type'] == 'checkbox') { ?>
                                    <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                        <label class="control-label"><?php echo $option['name']; ?></label>
                                        <div id="input-option<?php echo $option['product_option_id']; ?>">
                                            <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                                                        <?php echo $option_value['name']; ?>
                                                        <?php if ($option_value['price']) { ?>
                                                            (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                        <?php } ?>
                                                    </label>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php if ($option['type'] == 'image') { ?>
                                    <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                        <label class="control-label"><?php echo $option['name']; ?></label>
                                        <div id="input-option<?php echo $option['product_option_id']; ?>">
                                            <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                                                        <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" /> <?php echo $option_value['name']; ?>
                                                        <?php if ($option_value['price']) { ?>
                                                            (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                        <?php } ?>
                                                    </label>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php if ($option['type'] == 'text') { ?>
                                    <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                        <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                        <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                                    </div>
                                <?php } ?>
                                <?php if ($option['type'] == 'textarea') { ?>
                                    <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                        <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                        <textarea name="option[<?php echo $option['product_option_id']; ?>]" rows="5" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control"><?php echo $option['value']; ?></textarea>
                                    </div>
                                <?php } ?>
                                <?php if ($option['type'] == 'file') { ?>
                                    <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                        <label class="control-label"><?php echo $option['name']; ?></label>
                                        <button type="button" id="button-upload<?php echo $option['product_option_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default btn-block"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
                                        <input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="" id="input-option<?php echo $option['product_option_id']; ?>" />
                                    </div>
                                <?php } ?>
                                <?php if ($option['type'] == 'date') { ?>
                                    <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                        <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                        <div class="input-group date">
                                            <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                                            <span class="input-group-btn">
                    <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                    </span></div>
                                    </div>
                                <?php } ?>
                                <?php if ($option['type'] == 'datetime') { ?>
                                    <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                        <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                        <div class="input-group datetime">
                                            <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                                            <span class="input-group-btn">
                    <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                    </span></div>
                                    </div>
                                <?php } ?>
                                <?php if ($option['type'] == 'time') { ?>
                                    <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                        <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                        <div class="input-group time">
                                            <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                                            <span class="input-group-btn">
                    <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                    </span></div>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                        <?php } ?>
                        <?php if ($recurrings) { ?>
                            <hr>
                            <h3><?php echo $text_payment_recurring ?></h3>
                            <div class="form-group required">
                                <select name="recurring_id" class="form-control">
                                    <option value=""><?php echo $text_select; ?></option>
                                    <?php foreach ($recurrings as $recurring) { ?>
                                        <option value="<?php echo $recurring['recurring_id'] ?>"><?php echo $recurring['name'] ?></option>
                                    <?php } ?>
                                </select>
                                <div class="help-block" id="recurring-description"></div>
                            </div>
                        <?php } ?>
                        <?php if ($minimum > 1) { ?>
                            <div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $text_minimum; ?></div>
                        <?php } ?>
                    <?php } ?>
                </div>
                <div class="product-group_btn">
                    <?php if ($review_status) { ?>
                        <div class="rating">

                                <?php for ($i = 1; $i <= 5; $i++) { ?>
                                    <?php if ($rating < $i) { ?>
                                        <span class="fa "><i class="icon-star-grey"></i></span>
                                    <?php } else { ?>
                                        <span class="fa "><i class="icon-star-gold"></i></span>
                                    <?php } ?>
                                <?php } ?>
                                <a href="" onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;"><?php echo $reviews; ?></a>


                        </div>
                    <?php } ?>
                    <div class="btn-group">
                        <button type="button" class="btn-wish" onclick="compare.add('<?php echo $product_id; ?>');"><i class="icon-i-scale"></i></button>
                        <button type="button" class="btn-heart" onclick="wishlist.add('<?php echo $product_id; ?>');"><i class="icon-i-heart"></i></button>
                        <div class="addthis_counter icon-i-share btn-share">
                            <div class="share-social">
                                <a onclick="openWin2();" class="icon-i-fb"></a>
                                <a href="https://vk.com/share.php?url=<?php echo $og_url; ?>" target="_blank" class="icon-i-vk"></a>
                            </div>
                            <script>
                                $('.share-social .icon-i-fb').click(function (e) {
                                    e.preventDefault();
                                })
                                function openWin2() {
                                    myWin=open("https://www.facebook.com/sharer.php?u=<?php echo $og_url; ?>","displayWindow","width=520,height=300,left=350,top=170,status=no,toolbar=no,menubar=no");
                                }
                            </script>
                        </div>
                    </div>
              </div>
            </div>
      </div>
        <div class="product-tabs">
            <ul class="nav nav-tabs">
            <li class="active"><a href="#tab-description" data-toggle="tab"><?php echo $tab_description; ?></a></li>
            <?php if ($attribute_groups) { ?>
            <li><a href="#tab-specification" data-toggle="tab"><?php echo $tab_attribute; ?></a></li>
            <?php } ?>
            <?php if ($review_status) { ?>
            <li><a href="#tab-review" data-toggle="tab"><?php echo $tab_review; ?></a></li>
            <?php } ?>
            </ul>
            <div class="tab-content">
        <div class="tab-pane active" id="tab-description"><?php echo $description; ?></div>
        <?php if ($attribute_groups) { ?>
        <div class="tab-pane" id="tab-specification">
            <ul class="specification_list">
                <?php foreach ($attribute_groups as $attribute_group) { ?>
                    <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
                        <li>
                            <div class="specification_list-name"><?php echo $attribute['name']; ?></div>
                            <div class="specification_list-txt"><?php echo $attribute['text']; ?></div>
                        </li>
                    <?php } ?>
                <?php } ?>
            </ul>
        </div>
        <?php } ?>
        <?php if ($review_status) { ?>
            <div class="tab-pane" id="tab-review">
                <div class="product-review">
                    <div class="product-review-list" id="review">

                    </div>
                    <div class="product-review-form">
                        <form id="form-review">

                        <h2><?php echo $text_write; ?></h2>
                        <?php if ($review_guest) { ?>
                            <div class="product-review-input required">
                                    <label class="control-label" for="input-name-rev"><?php echo $entry_name; ?></label>
                                    <input type="text" name="name" value="" id="input-name-rev" class="form-control" />
                            </div>
                            <div class="product-review-input required">
                                <label class="control-label" for="txt_name"><?php echo $entry_mail; ?></label>
                                <input type="text" name="txt_name" value="" id="txt_name" class="form-control" />
                            </div>
                            <div class="product-review-input required">
                                <label class="control-label"><?php echo $entry_rating; ?></label>
                                <div class="product-review-radio">
                                    <input id="star-5" type="radio" name="rating" value="5"  />
                                    <label for="star-5" class="star icon-star-grey"></label>


                                    <input id="star-4" type="radio" name="rating" value="4" />
                                    <label for="star-4" class="star icon-star-grey"></label>


                                    <input id="star-3" type="radio" name="rating" value="3" />
                                    <label for="star-3" class="star icon-star-grey"></label>


                                    <input id="star-2" type="radio" name="rating" value="2" />
                                    <label for="star-2" class="star icon-star-grey"></label>


                                    <input id="star-1" type="radio" name="rating" value="1" />
                                    <label for="star-1" class="star icon-star-grey"></label>

                                </div>
                            </div>
                            <div class="product-review-input product-textarea required">
                                    <label class="control-label" for="input-review"><?php echo $entry_review; ?></label>
                                    <textarea name="text" rows="5" id="input-review" class="form-control"></textarea>
<!--                                    <div class="help-block">--><?php //echo $text_note; ?><!--</div>-->
                            </div>

                            <?php echo $captcha; ?>
                            <div class="product-review-buttons clearfix">
                                <button type="button" id="button-review" data-loading-text="<?php echo $text_loading; ?>">Отправить</button>
                            </div>
                        <?php } else { ?>
                            <?php echo $text_login; ?>
                        <?php } ?>
                    </form>
                    </div>
                </div>
            </div>
        <?php } ?>
        </div>
        </div>

      <?php /*if ($products) { ?>
      <h3><?php echo $text_related; ?></h3>
        <div class="featured-slider">
            <div class="featured-prev icon-arrow-prev"></div>
            <div class="slider">
                <?php $i = 0; ?>
                <?php foreach ($products as $product) { ?>
                    <?php $this->partial('product_item_module', array('product' => $product, 'button_cart' => $button_cart, 'text_tax' => $text_tax, 'button_wishlist' => $button_wishlist, 'button_compare' => $button_compare));?>
                <?php } ?>
            </div>
            <div class="featured-next icon-arrow-next"></div>
        </div>
        <script>
            $('.featured-slider .slider').slick({
                dots: false,
                infinite: true,
                speed: 300,
                slidesToShow: 4,
                slidesToScroll: 1,
                prevArrow: $('.featured-prev'),
                nextArrow: $('.featured-next'),
                responsive: [
                    {
                        breakpoint: 1200,
                        settings: {
                            slidesToShow: 3
                        }
                    },
                    {
                        breakpoint: 768,
                        settings: {
                            slidesToShow: 2
                        }
                    },
                    {
                        breakpoint: 600,
                        settings: {
                            slidesToShow: 1
                        }
                    }
                ]
            });
        </script>
      </div>
      <?php }*/ ?>
      <?php if ($tags) { ?>
      <p><?php echo $text_tags; ?>
        <?php for ($i = 0; $i < count($tags); $i++) { ?>
        <?php if ($i < (count($tags) - 1)) { ?>
        <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>,
        <?php } else { ?>
        <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>
        <?php } ?>
        <?php } ?>
      </p>
      <?php } ?>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<script><!--
$('select[name=\'recurring_id\'], input[name="quantity"]').change(function(){
	$.ajax({
		url: 'index.php?route=product/product/getRecurringDescription',
		type: 'post',
		data: $('input[name=\'product_id\'], input[name=\'quantity\'], select[name=\'recurring_id\']'),
		dataType: 'json',
		beforeSend: function() {
			$('#recurring-description').html('');
		},
		success: function(json) {
			$('.alert, .text-danger').remove();

			if (json['success']) {
				$('#recurring-description').html(json['success']);
			}
		}
	});
});
//--></script>
<script><!--
$('#button-cart').on('click', function() {
	$.ajax({
		url: 'index.php?route=checkout/cart/add',
		type: 'post',
		data: $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),
		dataType: 'json',
		beforeSend: function() {
			$('#button-cart').button('loading');
		},
		complete: function() {
			$('#button-cart').button('reset');
		},
		success: function(json) {
			$('.alert, .text-danger').remove();
			$('.form-group').removeClass('has-error');

			if (json['error']) {
				if (json['error']['option']) {
					for (i in json['error']['option']) {
						var element = $('#input-option' + i.replace('_', '-'));

						if (element.parent().hasClass('input-group')) {
							element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
						} else {
							element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
						}
					}
				}

				if (json['error']['recurring']) {
					$('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
				}

				// Highlight any found errors
				$('.text-danger').parent().addClass('has-error');
			}

			if (json['success']) {


                $('#content').parent().before('<div class="alert alert-main"><i class="fa fa-check-circle"></i> ' + json['success'] + ' </div>');

                setTimeout(function () {
                    $('.alert').detach();
                }, 5100);

				$('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');


				$('#cart > ul').load('index.php?route=common/cart/info ul li');
			}
		},
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
	});
});
//--></script>
<script><!--
$('.date').datetimepicker({
	pickTime: false
});

$('.datetime').datetimepicker({
	pickDate: true,
	pickTime: true
});

$('.time').datetimepicker({
	pickDate: false
});

$('button[id^=\'button-upload\']').on('click', function() {
	var node = this;

	$('#form-upload').remove();

	$('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

	$('#form-upload input[name=\'file\']').trigger('click');

	if (typeof timer != 'undefined') {
    	clearInterval(timer);
	}

	timer = setInterval(function() {
		if ($('#form-upload input[name=\'file\']').val() != '') {
			clearInterval(timer);

			$.ajax({
				url: 'index.php?route=tool/upload',
				type: 'post',
				dataType: 'json',
				data: new FormData($('#form-upload')[0]),
				cache: false,
				contentType: false,
				processData: false,
				beforeSend: function() {
					$(node).button('loading');
				},
				complete: function() {
					$(node).button('reset');
				},
				success: function(json) {
					$('.text-danger').remove();

					if (json['error']) {
						$(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
					}

					if (json['success']) {
						alert(json['success']);

						$(node).parent().find('input').attr('value', json['code']);
					}
				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		}
	}, 500);
});
//--></script>
<script><!--
$('#review').delegate('.pagination a', 'click', function(e) {
    e.preventDefault();

    $('#review').fadeOut('slow');

    $('#review').load(this.href);

    $('#review').fadeIn('slow');
});

$('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');

$('#button-review').on('click', function() {
	$.ajax({
		url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
		type: 'post',
		dataType: 'json',
		data: $("#form-review").serialize(),
		beforeSend: function() {
			$('#button-review').button('loading');
		},
		complete: function() {
			$('#button-review').button('reset');
		},
		success: function(json) {
			$('.alert-success, .alert-danger').remove();

			if (json['error']) {
				$('#form-review').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
			}

			if (json['success']) {
				$('#form-review').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

				$('input[name=\'name\']').val('');
				$('textarea[name=\'text\']').val('');
				$('input[name=\'rating\']:checked').prop('checked', false);
			}
		}
	});
});

$(document).ready(function() {
	$('.thumbnails').magnificPopup({
		type:'image',
		delegate: 'a',
		gallery: {
			enabled:true
		}
	});
});
//--></script>
<?php echo $footer; ?>
