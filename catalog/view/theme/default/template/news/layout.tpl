<?php echo $header; ?>
<div class="container">
        <?php $this->partial('breadcrumbs', array('breadcrumbs' => $breadcrumbs));?>
    <h1><?php echo $heading_title; ?></h1>
    <div id="content" class="row">
         <?php echo $description; ?>
    </div>
</div>

<?php echo $footer; ?> 
