<?php echo $header; ?>
    <div class="wrap_bread">
        <div class="container">
            <ul  class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
                <?php foreach ($breadcrumbs as $i=> $breadcrumb) { ?>
                    <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                        <?php if($i+1<count($breadcrumbs)) { ?>
                            <a itemscope itemtype="http://schema.org/Thing" itemprop="item" href="<?php echo $breadcrumb['href']; ?>">
                                <span itemprop="name"><?php echo $breadcrumb['text']; ?></span></a>
                        <?php } else { ?><span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
                        <?php } ?>
                    </li>
                <?php } ?>
            </ul>
        </div>
    </div>
<div class="container">
  <div class="row">
    <div id="content" class="col-sm-12"><?php echo $content_top; ?>
      <h1><?php echo $heading_title; ?></h1>
        <?php if($images){ ?>
            <div class="row gallery_list">
                <?php foreach ($images as $image){ ?>
                        <div class="col-sm-4">
                            <div class="gallery_item">
                                <a href="<?= $image['thumb']; ?>" class="thumbnail" data-fancybox="gallery">
                                    <img src="<?= $image['image']; ?>" alt="<?= $image['title']; ?>" class="img-responsive">
                                </a>
                            </div>
                        </div>
                <?php } ?>
            </div>
        <?php }else{ ?>
            <p><?= $text_empty; ?></p>
            <div class="text-right">
                <a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a>
            </div>
        <?php } ?>
        <div class="row">
            <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
            <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        </div>
        <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>