<?php echo $header; ?>
<div class="container information-information">
    <?php $this->partial('breadcrumbs', array('breadcrumbs' => $breadcrumbs));?>
  <h1><?php echo $heading_title; ?></h1>
  <div id="content" class="row page_contact">
<?php echo $column_left; ?>
<?php echo $content_top; ?>
      <div class="col-md-8 col-sm-7">
          <div  id="map" class="page_contact-map" data-point="<?php echo $data['map-point']; ?>"></div>
          <script>
              if($('#map').length){
                  init($('#map').data('point'));
              }
              function init(point) {
                  var point_y = point.slice(0, point.indexOf(','));
                  var point_x = point.slice(point.indexOf(' '),point.length);
                  console.log(point_x);
                  var mapOptions = {
                      zoom: 11,
                      center: new google.maps.LatLng(point_y, point_x)
                  };
                  var mapElement = document.getElementById('map');
                  var map = new google.maps.Map(mapElement, mapOptions);
                  var marker = new google.maps.Marker({
                      position: new google.maps.LatLng(point_y, point_x),
                      map: map,
                      title: 'Augusta Apartments\n' +
                          'Eichstrasse 6\n' +
                          '76530 Baden-Baden'
                  });
              }
          </script>
      </div>
      <div class="col-md-4 col-sm-5">
          <div class="page_contact-info">
              <div class="page_contact-adress">
                  <i class="icon-i-map"></i>
                  <?php echo $address; ?>
              </div>
              <div class="page_contact-mail">
                  <i class="icon-i-mail"></i>
                  <a href="email:<?php echo $email ?>"><?php echo $email ?></a>
              </div>
              <div class="page_contact-phone">
                  <?php if ($telephone1) { ?>
                      <a class="header-phone-main" href="tel:<?php echo preg_replace("/[ ()-]/", "", $telephone1) ;?>">
                          <i class="icon-i-lifecell"></i>
                          <?php echo $telephone1; ?>
                      </a>
                  <?php }?>
                  <?php if ($telephone2) { ?>
                      <a class="header-phone-kv" href="tel:<?php echo preg_replace("/[ ()-]/", "", $telephone2) ;?>">
                          <i class="icon-i-ks"></i>
                          <?php echo $telephone2; ?>
                      </a>
                  <?php }?>
                  <?php if ($telephone3) { ?>
                      <a class="header-phone-mts" href="tel:<?php echo preg_replace("/[ ()-]/", "", $telephone3) ;?>">
                          <i class="icon-i-mts"></i>
                          <?php echo $telephone3; ?>
                      </a>
                  <?php }?>
                  <?php if ($telephone4) { ?>
                      <a class="header-phone-life" href="tel:<?php echo preg_replace("/[ ()-]/", "", $telephone4) ;?>">
                          <i class="icon-i-phone"></i>
                          <?php echo $telephone4; ?>
                      </a>
                  <?php }?>
              </div>
              <div class="page_contact-date_work">
                  <i class="icon-i-clock"></i>
                  <?php echo $data_work ?>
              </div>
              <div class="page_contact-social">
                  <?php if ($telegram) { ?><a href="<?php echo $telegram; ?>" class="icon-i-insta" target="_blank"></a><?php } ?>
                  <?php if ($fb) { ?><a href="<?php echo $fb; ?>" class="icon-i-fb" target="_blank"></a><?php } ?>
                  <?php if ($vk) { ?><a href="<?php echo $vk; ?>" class="icon-i-vk" target="_blank"></a><?php } ?>
              </div>
              <?php if ($locations) { ?>
                  <h3><?php echo $text_store; ?></h3>
                  <div class="panel-group" id="accordion">
                      <?php foreach ($locations as $location) { ?>
                          <div class="panel panel-default">
                              <div class="panel-heading">
                                  <h4 class="panel-title"><a href="#collapse-location<?php echo $location['location_id']; ?>" class="accordion-toggle" data-toggle="collapse" data-parent="#accordion"><?php echo $location['name']; ?> <i class="fa fa-caret-down"></i></a></h4>
                              </div>
                              <div class="panel-collapse collapse" id="collapse-location<?php echo $location['location_id']; ?>">
                                  <div class="panel-body">
                                      <div class="row">
                                          <?php if ($location['image']) { ?>
                                              <div class="col-sm-3"><img src="<?php echo $location['image']; ?>" alt="<?php echo $location['name']; ?>" title="<?php echo $location['name']; ?>" class="img-thumbnail" /></div>
                                          <?php } ?>
                                          <div class="col-sm-3"><strong><?php echo $location['name']; ?></strong><br />
                                              <address>
                                                  <?php echo $location['address']; ?>
                                              </address>
                                              <?php if ($location['geocode']) { ?>
                                                  <a href="https://maps.google.com/maps?q=<?php echo urlencode($location['geocode']); ?>&hl=<?php echo $geocode_hl; ?>&t=m&z=15" target="_blank" class="btn btn-info"><i class="fa fa-map-marker"></i> <?php echo $button_map; ?></a>
                                              <?php } ?>
                                          </div>
                                          <div class="col-sm-3"> <strong><?php echo $text_telephone; ?></strong><br>
                                              <?php echo $location['telephone']; ?><br />
                                              <br />
                                              <?php if ($location['fax']) { ?>
                                                  <strong><?php echo $text_fax; ?></strong><br>
                                                  <?php echo $location['fax']; ?>
                                              <?php } ?>
                                          </div>
                                          <div class="col-sm-3">
                                              <?php if ($location['open']) { ?>
                                                  <strong><?php echo $text_open; ?></strong><br />
                                                  <?php echo $location['open']; ?><br />
                                                  <br />
                                              <?php } ?>
                                              <?php if ($location['comment']) { ?>
                                                  <strong><?php echo $text_comment; ?></strong><br />
                                                  <?php echo $location['comment']; ?>
                                              <?php } ?>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      <?php } ?>
                  </div>
              <?php } ?>
          </div>
      </div>
<?php echo $content_bottom; ?>
  </div>
<?php echo $column_right; ?>
</div>
</div>
<?php echo $footer; ?>
