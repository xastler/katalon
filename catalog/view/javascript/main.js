$(document).ready(function () {
    $('.top_bar-btn').click(function () {
        $(this).toggleClass('active');
        $('.top_bar-menu').slideToggle();
    });
    $('.categories_menu-btn').click(function () {
        $('.categories_menu-btn-span').toggleClass('active');
        $('.categories_menu').toggleClass('open');
        $('.categories_menu-block').slideToggle();
    });
    $('.popups_log-header li').click(function () {
        $('.popups_log-header li').removeClass('active');
        $(this).addClass('active');
        $('.popups_log-form').removeClass('active');
        $('.' + $(this).data('href')).addClass('active');
    });

    $('.header #search').click(function () {
        $('.header #search').addClass('active');
        $('.header #logo').addClass('active');
    });

    $('.btn-share').click(function () {
        $('.btn-share .share-social').addClass('open');
    });

    $('.btn-share').hover(function () {
        $('.btn-share .share-social').toggleClass('open');
    });


    $('.mfilter-xs').click(function () {
        $(this).toggleClass('active');
        $(this).next().slideToggle();
    });
    $('.simplecheckout-step .simplecheckout-left-column').prepend('<h2>Оформление заказа</h2>');
    $('.simplecheckout-step .simplecheckout-right-column').prepend('<h2>ТОВАРЫ</h2>');

    jQuery(function () {
        var j = jQuery; //Just a variable for using jQuery without conflicts
        var addInput = '#input-quantity'; //This is the id of the input you are changing
        var n = 1; //n is equal to 1

        //Set default value to n (n = 1)
        j(addInput).val(n);

        //On click add 1 to n
        j('.plus').on('click', function () {
            j(addInput).val(++n);
        })

        j('.minus').on('click', function () {
            //If n is bigger or equal to 1 subtract 1 from n
            if (n >= 2) {
                j(addInput).val(--n);
            } else {
                //Otherwise do nothing
            }
        });
    });
    $('select').styler();

    $('.checkbox22 input[type=checkbox]').on('change', function () {
        var $el = $(this);
        if ($el.is(':checked')) {
            $('#input-password-l').attr('type', 'text');
        } else {
            $('#input-password-l').attr('type', 'password');
        }
    });
    $('.checkbox33 input[type=checkbox]').on('change', function () {
        var $el = $(this);
        if ($el.is(':checked')) {
            $('#input-password-r').attr('type', 'text');
        } else {
            $('#input-password-r').attr('type', 'password');
        }
    });
    $('input[type=tel], input[name="phone"], #customer_telephone, input[name="telephone"]').mask('+38 (999) 999-99-99');

    var offset = $('.categories_menu').offset();

    var topPadding = 0;
    $(window).scroll(function () {
        if ($(window).scrollTop() > offset.top) {
            $('.categories_menu').addClass('fixed');
        }
        else {
            $('.categories_menu').removeClass('fixed');
        }
    });



});
var offset = $('.categories_menu').offset();
if ($(window).width() <= 480) {
    $('body').animate({scrollTop: offset.top}, 1100);
}
$(document).delegate('.quick_login', 'click', function (e) {
    $('#modal-login').modal('show');
});
$('#quick-register input').on('keydown', function (e) {
    if (e.keyCode == 13) {
        $('#quick-register .createaccount').trigger('click');
    }
});
$('#quick-register .createaccount').click(function () {
    $.ajax({
        url: 'index.php?route=common/quicksignup/register',
        type: 'post',
        data: $('#quick-register input[type=\'text\'], #quick-register input[type=\'password\'], #quick-register input[type=\'checkbox\']:checked'),
        dataType: 'json',
        beforeSend: function () {
            $('#quick-register .createaccount').button('loading');
            $('#quick-register .alert-danger').remove();
        },
        complete: function () {
            $('#quick-register .createaccount').button('reset');
        },
        success: function (json) {
            $('#quick-register .form-group').removeClass('has-error');

            if (json['islogged']) {
                window.location.href = "index.php?route=account/account";
            }
            if (json['error_name']) {
                $('#quick-register #input-name-r').parent().addClass('has-error');
                $('#quick-register #input-name-r').after('<div class="alert alert-danger" ><i class="fa fa-exclamation-circle"></i> ' + json['error_name'] + '</div>');
                $('#quick-register #input-name-r').focus();
            }
            if (json['error_email']) {
                $('#quick-register #input-email-r').parent().addClass('has-error');
                $('#quick-register #input-email-r').after('<div class="alert alert-danger" ><i class="fa fa-exclamation-circle"></i> ' + json['error_email'] + '</div>');
                $('#quick-register #input-email-r').focus();
            }
            if (json['error_telephone']) {
                $('#quick-register #input-telephone-r').parent().addClass('has-error');
                $('#quick-register #input-telephone-r').after('<div class="alert alert-danger" ><i class="fa fa-exclamation-circle"></i> ' + json['error_telephone'] + '</div>');
                $('#quick-register #input-telephone-r').focus();
            }
            if (json['error_password']) {
                $('#quick-register #input-password-r').parent().addClass('has-error');
                $('#quick-register #input-password-r').after('<div class="alert alert-danger" ><i class="fa fa-exclamation-circle"></i> ' + json['error_password'] + '</div>');
                $('#quick-register #input-password-r').focus();
            }

            if (json['now_login']) {
                $('.quick-login').before('<li class="dropdown"><a href="<?php echo $account; ?>" title="<?php echo $text_account; ?>" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_account; ?></span> <span class="caret"></span></a><ul class="dropdown-menu dropdown-menu-right"><li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li><li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li><li><a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></li><li><a href="<?php echo $download; ?>"><?php echo $text_download; ?></a></li><li><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li></ul></li>');

                $('.quick-login').remove();
            }
            if (json['success']) {
                $('#quick-register .popups_log-form').html(json['heading_title']);
                success = json['text_message'];
                success += '<div class="buttons"><div class="text-right"><a onclick="loacation();" class="btn btn-primary">' + json['button_continue'] + '</a></div></div>';
                $('#quick-register .modal-body').html(success);
            }
        }
    });
});
$('#quick-login input').on('keydown', function (e) {
    if (e.keyCode == 13) {
        $('#quick-login .loginaccount').trigger('click');
    }
});
$('#quick-login .loginaccount').click(function () {
    $.ajax({
        url: 'index.php?route=common/quicksignup/login',
        type: 'post',
        data: $('#quick-login input[type=\'text\'], #quick-login input[type=\'password\']'),
        dataType: 'json',
        beforeSend: function () {
            $('#quick-login .loginaccount').button('loading');
            $('#modal-login .alert-danger').remove();
        },
        complete: function () {
            $('#quick-login .loginaccount').button('reset');
        },
        success: function (json) {
            $('#modal-login .form-group').removeClass('has-error');
            if (json['islogged']) {
                window.location.href = "index.php?route=account/account";
            }

            if (json['error']) {
                $('#modal-login #input-password-l').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
                $('#quick-login #input-password-l').parent().addClass('has-error');
                $('#quick-login #input-email-l').focus();
            }
            if (json['success']) {
                loacation();
                $('#modal-login').modal('hide');
            }

        }
    });
});

function loacation() {
    location.reload();
}


