<?php
class ControllerModuleDopbuy extends Controller {
	public function index($setting) {
		$this->load->language('module/dop_buy');

		$data['heading_title'] = $this->language->get('heading_title');
        $data['button_cart'] = $this->language->get('button_cart');
        $data['button_details'] = $this->language->get('button_details');

		$this->load->model('catalog/category');

		$this->load->model('catalog/product');
		
		$this->load->model('tool/image');


		$cart_products = $this->cart->getProducts();



		foreach ($cart_products as $product)
        {
           $cart_related_products = $this->model_catalog_product->getProductRelated($product['product_id']);
        }



		if (isset($cart_related_products)) {
			foreach ($cart_related_products as $result) {


				if ($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], $setting['width'], $setting['height']);
                } else {
                    $image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
                }

				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				} else {
					$price = false;
				}

				//var_dump($price);


				if ((float)$result['special']) {
					$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$special = false;
				}

				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price']);
				} else {
					$tax = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = $result['rating'];
				} else {
					$rating = false;
				}
				if(utf8_strlen($result['name']) < 80){
					$sub_name = $result['name'];
				}else{
					$sub_name = utf8_substr($result['name'], 0, 80) . '..';
				}

                if (isset($this->session->data['wishlist']) && in_array($result['product_id'], $this->session->data['wishlist'])) {
                    $wishlist_status = '1';
                } else {
                    $wishlist_status = false;
                }

                if (isset($this->session->data['compare']) && in_array($result['product_id'], $this->session->data['compare'])) {
                    $compare_status = '1';
                } else {
                    $compare_status = false;
                }


				$data['products'][] = array(
					'product_id'  => $result['product_id'],
					'thumb'       => $image,
					'name'        => $sub_name ,
					'wishlist_status' => $wishlist_status ,
					'compare_status' => $compare_status ,
					'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
					'price'       => $price,
					'special'     => $special,
					'tax'         => $tax,
					'rating'      => $rating,
					'href'        => $this->url->link('product/product', 'product_id=' . $result['product_id'])
				);
			}
		}
        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/dop_buy.tpl')) {
            return $this->load->view($this->config->get('config_template') . '/template/module/dop_buy.tpl', $data);
        } else {
            return $this->load->view('default/template/module/special.tpl', $data);
        }
	}
}