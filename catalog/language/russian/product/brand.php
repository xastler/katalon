<?php
// Heading
$_['heading_title']     = 'Список Популярных Брендов';
$_['heading_title_module']     = 'Популярные Бренды';

// Text
$_['text_brand']        = 'Популярные Бренды';
$_['text_index']        = 'Алфавитный указатель:';
$_['text_error']        = 'Брендов не найден!';
$_['text_empty']        = 'Нет товаров для отображения.';
$_['text_quantity']     = 'Кол-во:';
$_['text_manufacturer'] = 'Брендов:';
$_['text_model']        = 'Код товара:';
$_['text_points']       = 'Бонусные баллы:';
$_['text_price']        = 'Цена:';
$_['text_tax']          = 'Без налога:';
$_['text_compare']      = 'Сравнение товаров (%s)';
$_['text_sort']         = 'Сортировать:';
$_['text_default']      = 'По умолчанию';
$_['text_name_asc']     = 'По Имени (A - Я)';
$_['text_name_desc']    = 'По Имени (Я - A)';
$_['text_price_asc']    = 'По Цене (возрастанию)';
$_['text_price_desc']   = 'По Цене (убыванию)';
$_['text_rating_asc']   = 'По Рейтингу (возрастанию)';
$_['text_rating_desc']  = 'По Рейтингу (убыванию)';
$_['text_model_asc']    = 'По Модели (A - Z)';
$_['text_model_desc']   = 'По Модели (Z - A)';
$_['text_limit']        = 'Показывать:';
