<?php
// Heading 
$_['heading_title'] 		 = 'Подпишитесь';

// Text
$_['text_intro']	 		 = '';
$_['text_description']	 	 = 'Введите ваш Еmail, чтобы регулярно получать новости';
$_['text_subscribed']   	 = 'Подписка оформлена!';
$_['text_unsubscribed']   	 = 'Вы уже подписаны!';
$_['text_subject'] 			 = 'Подписка';
$_['text_message'] 			 = 'Email: %s';

//Fields
$_['entry_email'] 			 = 'Введите Еmail';

//Buttons
$_['text_button'] 			 = 'Подписаться';

//Error
$_['error_invalid'] 		 = 'Некорректный e-mail!';
