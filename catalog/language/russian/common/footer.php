<?php
// Text
$_['text_information']  = 'МАГАЗИН';
$_['text_service']      = 'Служба поддержки';
$_['text_extra']        = 'ПОМОЩЬ';
$_['text_contact']      = 'Контакты';
$_['text_return']       = 'Возврат товара';
$_['text_sitemap']      = 'Карта сайта';
$_['text_manufacturer'] = 'Производители';
$_['text_voucher']      = 'Подарочные сертификаты';
$_['text_affiliate']    = 'Партнёры';
$_['text_special']      = 'Акция';
$_['text_account']      = 'Аккаунт';
$_['text_order']        = 'История заказов';
$_['text_wishlist']     = 'Вишлист';
$_['text_newsletter']   = 'Рассылка новостей';
$_['text_powered']      = '%s Copyright &copy; <a href="/">Katalano</a>. Все права защищены';
$_['entry_name']      	= 'Имя';
$_['entry_phone']       = 'Номер мобильного телефона';
$_['text_call']      	= 'Заказать звонок';
$_['text_send']      	= 'Отправить';
$_['text_loading']      = 'Обработка';
$_['text_compare1']      = 'Сравнение товаров';
$_['text_checkout1']     = 'Корзина';