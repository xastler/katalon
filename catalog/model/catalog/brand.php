<?php
class ModelCatalogBrand extends Model {
	public function getBrand($brand_id) {
		$query = $this->db->query("SELECT DISTINCT *, md.name AS name FROM " . DB_PREFIX . "brand m LEFT JOIN " . DB_PREFIX . "brand_description md ON (m.brand_id = md.brand_id) LEFT JOIN " . DB_PREFIX . "brand_to_store m2s ON (m.brand_id = m2s.brand_id) WHERE md.language_id = '" . (int)$this->config->get('config_language_id') . "' && m.brand_id = '" . (int)$brand_id . "' AND m2s.store_id = '" . (int)$this->config->get('config_store_id') . "'");

		return $query->row;
	}

	public function getBrands($data = array()) {
		if ($data) {
			$sql = "SELECT * FROM " . DB_PREFIX . "brand m LEFT JOIN " . DB_PREFIX . "brand_to_store m2s ON (m.brand_id = m2s.brand_id) LEFT JOIN " . DB_PREFIX . "brand_description md ON (m.brand_id = md.brand_id) WHERE md.language_id = '" . (int)$this->config->get('config_language_id') . "' && m2s.store_id = '" . (int)$this->config->get('config_store_id') . "'";

			$sort_data = array(
				'name',
				'sort_order'
			);

            if (isset($data['home_show'])) {
                $sql .= " AND m.home_show = '".$data['home_show']."'";
            }

			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];
			} else {
				$sql .= " ORDER BY md.name";
			}

			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}

			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}

				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}

			$query = $this->db->query($sql);

			return $query->rows;
		} else {
			$brand_data = $this->cache->get('brand.' . (int)$this->config->get('config_language_id').'.'. (int)$this->config->get('config_store_id'));

			if (!$brand_data) {
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "brand m LEFT JOIN " . DB_PREFIX . "brand_to_store m2s ON (m.brand_id = m2s.brand_id) LEFT JOIN " . DB_PREFIX . "brand_description md ON (m.brand_id = md.brand_id) WHERE md.language_id = '" . (int)$this->config->get('config_language_id') . "' && m2s.store_id = '" . (int)$this->config->get('config_store_id') . "' ORDER BY md.name");

				$brand_data = $query->rows;

				$this->cache->set('brand.' . (int)$this->config->get('config_language_id') . '.'. (int)$this->config->get('config_store_id'), $brand_data);
			}

			return $brand_data;
		}
	}
}
