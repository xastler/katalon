<?php
class ModelCatalogBrand extends Model {
	public function addBrand($data) {
		$this->event->trigger('pre.admin.brand.add', $data);

		$this->load->model('localisation/language');
		$language_info = $this->model_localisation_language->getLanguageByCode($this->config->get('config_language'));
    $front_language_id = $language_info['language_id'];
		$data['name'] = $data['brand_description'][$front_language_id ]['name'];
		
		$this->db->query("INSERT INTO " . DB_PREFIX . "brand SET name = '" . $this->db->escape($data['name']) . "', sort_order = '" . (int)$data['sort_order'] . "', home_show = '" . (int)$data['home_show'] . "'");

		$brand_id = $this->db->getLastId();

		if (isset($data['image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "brand SET image = '" . $this->db->escape($data['image']) . "' WHERE brand_id = '" . (int)$brand_id . "'");
		}

		foreach ($data['brand_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "brand_description SET brand_id = '" . (int)$brand_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($value['description']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_h1 = '" . $this->db->escape($value['meta_h1']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
		}

		if (isset($data['brand_store'])) {
			foreach ($data['brand_store'] as $store_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "brand_to_store SET brand_id = '" . (int)$brand_id . "', store_id = '" . (int)$store_id . "'");
			}
		}

        /*mod create alias*/
        if ((isset($data['keyword'])) and (!empty($data['keyword']))) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'brand_id=" . (int)$brand_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
        }	else {
            $this->load->model('tool/alias');
            $name_to_alias = $data['brand_description'][$this->config->get('config_language_id')]['name'];
            $name = $this->model_tool_alias->translitIt(str_replace(' ', '-', trim($this->db->escape($name_to_alias))), ENT_QUOTES, "UTF-8").'-'.$brand_id;
            $this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'brand_id=" . (int)$brand_id . "', keyword = '" . $name . "'");
        }
        $this->cache->delete('seo_pro');

        /*!mod create alias*/

        $this->cache->delete('brand');

		$this->event->trigger('post.admin.brand.add', $brand_id);

		return $brand_id;
	}

	public function editBrand($brand_id, $data) {
		$this->event->trigger('pre.admin.brand.edit', $data);

		$this->load->model('localisation/language');
		$language_info = $this->model_localisation_language->getLanguageByCode($this->config->get('config_language'));
    $front_language_id = $language_info['language_id'];
		$data['name'] = $data['brand_description'][$front_language_id ]['name'];

		$this->db->query("UPDATE " . DB_PREFIX . "brand SET name = '" . $this->db->escape($data['name']) . "', sort_order = '" . (int)$data['sort_order'] . "', home_show = '" . (int)$data['home_show'] . "' WHERE brand_id = '" . (int)$brand_id . "'");

		if (isset($data['image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "brand SET image = '" . $this->db->escape($data['image']) . "' WHERE brand_id = '" . (int)$brand_id . "'");
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "brand_description WHERE brand_id = '" . (int)$brand_id . "'");

		foreach ($data['brand_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "brand_description SET brand_id = '" . (int)$brand_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($value['description']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_h1 = '" . $this->db->escape($value['meta_h1']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "brand_to_store WHERE brand_id = '" . (int)$brand_id . "'");

		if (isset($data['brand_store'])) {
			foreach ($data['brand_store'] as $store_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "brand_to_store SET brand_id = '" . (int)$brand_id . "', store_id = '" . (int)$store_id . "'");
			}
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'brand_id=" . (int)$brand_id . "'");

        /*mod create alias*/
        if ((isset($data['keyword'])) and (!empty($data['keyword']))) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'brand_id=" . (int)$brand_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
        }	else {
            $this->load->model('tool/alias');
            $name_to_alias = $data['brand_description'][$this->config->get('config_language_id')]['name'];
            $name = $this->model_tool_alias->translitIt(str_replace(' ', '-', trim($this->db->escape($name_to_alias))), ENT_QUOTES, "UTF-8").'-'.$brand_id;
            $this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'brand_id=" . (int)$brand_id . "', keyword = '" . $name . "'");
        }
        $this->cache->delete('seo_pro');

        /*!mod create alias*/
		$this->cache->delete('brand');

		$this->event->trigger('post.admin.brand.edit', $brand_id);
	}

	public function deleteBrand($brand_id) {
		$this->event->trigger('pre.admin.brand.delete', $brand_id);

		$this->db->query("DELETE FROM " . DB_PREFIX . "brand WHERE brand_id = '" . (int)$brand_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "brand_to_store WHERE brand_id = '" . (int)$brand_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'brand_id=" . (int)$brand_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "brand_description WHERE brand_id = '" . (int)$brand_id . "'");

		$this->cache->delete('brand');

		$this->event->trigger('post.admin.brand.delete', $brand_id);
	}

	public function getBrand($brand_id) {
		$query = $this->db->query("SELECT DISTINCT *, (SELECT keyword FROM " . DB_PREFIX . "url_alias WHERE query = 'brand_id=" . (int)$brand_id . "') AS keyword FROM " . DB_PREFIX . "brand WHERE brand_id = '" . (int)$brand_id . "'");

		return $query->row;
	}

	public function getBrandDescriptions($brand_id) {
		$brand_description_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "brand_description WHERE brand_id = '" . (int)$brand_id . "'");

		foreach ($query->rows as $result) {
			$brand_description_data[$result['language_id']] = array(
				'name'             => $result['name'],
				'meta_title'       => $result['meta_title'],
				'meta_h1'          => $result['meta_h1'],
				'meta_description' => $result['meta_description'],
				'meta_keyword'     => $result['meta_keyword'],
				'description'      => $result['description']
			);
		}

		return $brand_description_data;
	}

	public function getBrands($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "brand";

		$sql = "SELECT c.brand_id, md.name, c.sort_order FROM " . DB_PREFIX . "brand c LEFT JOIN " . DB_PREFIX . "brand_description md ON (c.brand_id = md.brand_id) WHERE md.language_id = '" . (int)$this->config->get('config_language_id') . "'";



		if (!empty($data['filter_name'])) {
			$sql .= " AND name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
		}

		$sort_data = array(
			'name',
			'sort_order'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getBrandStores($brand_id) {
		$brand_store_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "brand_to_store WHERE brand_id = '" . (int)$brand_id . "'");

		foreach ($query->rows as $result) {
			$brand_store_data[] = $result['store_id'];
		}

		return $brand_store_data;
	}

	public function getTotalBrands() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "brand");

		return $query->row['total'];
	}
}
